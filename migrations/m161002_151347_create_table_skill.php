<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%skill}}`.
 */
class m161002_151347_create_table_skill extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%skill}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45),
            'info' => $this->string(45),
            'img' => $this->string(45),
            'lvl' => $this->string(45),
            'type_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `type_id`
        $this->createIndex(
            'fk_skill_type1',
            '{{%skill}}',
            'type_id'
        );

        // add foreign key for table `type`
        $this->addForeignKey(
            'fk_skill_type1',
            '{{%skill}}',
            'type_id',
            '{{%type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `type`
        $this->dropForeignKey(
            'fk_skill_type1',
            '{{%skill}}'
        );

        // drops index for column `type_id`
        $this->dropIndex(
            'fk_skill_type1',
            '{{%skill}}'
        );

        $this->dropTable('{{%skill}}');
    }
}
