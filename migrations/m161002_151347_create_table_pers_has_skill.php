<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers_has_skill}}`.
 */
class m161002_151347_create_table_pers_has_skill extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers_has_skill}}', [

            'id' => $this->primaryKey()->notNull(),
            'pers_id' => $this->integer(11)->notNull(),
            'skill_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `pers_id`
        $this->createIndex(
            'fk_pers_has_skill_pers1',
            '{{%pers_has_skill}}',
            'pers_id'
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'fk_pers_has_skill_pers1',
            '{{%pers_has_skill}}',
            'pers_id',
            '{{%pers}}',
            'id',
            'CASCADE'
        );

        // creates index for column `skill_id`
        $this->createIndex(
            'fk_pers_has_skill_skill1',
            '{{%pers_has_skill}}',
            'skill_id'
        );

        // add foreign key for table `skill`
        $this->addForeignKey(
            'fk_pers_has_skill_skill1',
            '{{%pers_has_skill}}',
            'skill_id',
            '{{%skill}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'fk_pers_has_skill_pers1',
            '{{%pers_has_skill}}'
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'fk_pers_has_skill_pers1',
            '{{%pers_has_skill}}'
        );

        // drops foreign key for table `skill`
        $this->dropForeignKey(
            'fk_pers_has_skill_skill1',
            '{{%pers_has_skill}}'
        );

        // drops index for column `skill_id`
        $this->dropIndex(
            'fk_pers_has_skill_skill1',
            '{{%pers_has_skill}}'
        );

        $this->dropTable('{{%pers_has_skill}}');
    }
}
