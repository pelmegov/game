<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%user_has_pers}}`.
 */
class m161002_151348_create_table_user_has_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%user_has_pers}}', [

            'id' => $this->primaryKey()->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'pers_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `user_id`
        $this->createIndex(
            'fk_user_has_pers_user',
            '{{%user_has_pers}}',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_user_has_pers_user',
            '{{%user_has_pers}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `pers_id`
        $this->createIndex(
            'fk_user_has_pers_pers1',
            '{{%user_has_pers}}',
            'pers_id'
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'fk_user_has_pers_pers1',
            '{{%user_has_pers}}',
            'pers_id',
            '{{%pers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk_user_has_pers_user',
            '{{%user_has_pers}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'fk_user_has_pers_user',
            '{{%user_has_pers}}'
        );

        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'fk_user_has_pers_pers1',
            '{{%user_has_pers}}'
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'fk_user_has_pers_pers1',
            '{{%user_has_pers}}'
        );

        $this->dropTable('{{%user_has_pers}}');
    }
}
