<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers_has_param}}`.
 */
class m161002_151347_create_table_pers_has_param extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers_has_param}}', [

            'id' => $this->primaryKey()->notNull(),
            'pers_id' => $this->integer(11)->notNull(),
            'param_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `pers_id`
        $this->createIndex(
            'fk_pers_has_param_pers1',
            '{{%pers_has_param}}',
            'pers_id'
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'fk_pers_has_param_pers1',
            '{{%pers_has_param}}',
            'pers_id',
            '{{%pers}}',
            'id',
            'CASCADE'
        );

        // creates index for column `param_id`
        $this->createIndex(
            'fk_pers_has_param_param1',
            '{{%pers_has_param}}',
            'param_id'
        );

        // add foreign key for table `param`
        $this->addForeignKey(
            'fk_pers_has_param_param1',
            '{{%pers_has_param}}',
            'param_id',
            '{{%param}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'fk_pers_has_param_pers1',
            '{{%pers_has_param}}'
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'fk_pers_has_param_pers1',
            '{{%pers_has_param}}'
        );

        // drops foreign key for table `param`
        $this->dropForeignKey(
            'fk_pers_has_param_param1',
            '{{%pers_has_param}}'
        );

        // drops index for column `param_id`
        $this->dropIndex(
            'fk_pers_has_param_param1',
            '{{%pers_has_param}}'
        );

        $this->dropTable('{{%pers_has_param}}');
    }
}
