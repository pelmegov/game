<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%param}}`.
 */
class m161002_151347_create_table_param extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%param}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45),
            'info' => $this->string(255),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%param}}');
    }
}
