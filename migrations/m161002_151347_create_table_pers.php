<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers}}`.
 */
class m161002_151347_create_table_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45)->notNull(),
            'lvl' => $this->integer(11)->notNull(),
            'exp' => $this->integer(11)->notNull(),
            'img' => $this->string(255),
            'story_id' => $this->integer(11)->notNull(),
            'castle_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `story_id`
        $this->createIndex(
            'fk_pers_story1',
            '{{%pers}}',
            'story_id'
        );

        // add foreign key for table `story`
        $this->addForeignKey(
            'fk_pers_story1',
            '{{%pers}}',
            'story_id',
            '{{%story}}',
            'id',
            'CASCADE'
        );

        // creates index for column `castle_id`
        $this->createIndex(
            'fk_pers_castle1',
            '{{%pers}}',
            'castle_id'
        );

        // add foreign key for table `castle`
        $this->addForeignKey(
            'fk_pers_castle1',
            '{{%pers}}',
            'castle_id',
            '{{%castle}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `story`
        $this->dropForeignKey(
            'fk_pers_story1',
            '{{%pers}}'
        );

        // drops index for column `story_id`
        $this->dropIndex(
            'fk_pers_story1',
            '{{%pers}}'
        );

        // drops foreign key for table `castle`
        $this->dropForeignKey(
            'fk_pers_castle1',
            '{{%pers}}'
        );

        // drops index for column `castle_id`
        $this->dropIndex(
            'fk_pers_castle1',
            '{{%pers}}'
        );

        $this->dropTable('{{%pers}}');
    }
}
