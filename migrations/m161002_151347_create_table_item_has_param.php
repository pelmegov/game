<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%item_has_param}}`.
 */
class m161002_151347_create_table_item_has_param extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%item_has_param}}', [

            'id' => $this->primaryKey()->notNull(),
            'item_id' => $this->integer(11)->notNull(),
            'param_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `item_id`
        $this->createIndex(
            'fk_item_has_param_item1',
            '{{%item_has_param}}',
            'item_id'
        );

        // add foreign key for table `item`
        $this->addForeignKey(
            'fk_item_has_param_item1',
            '{{%item_has_param}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );

        // creates index for column `param_id`
        $this->createIndex(
            'fk_item_has_param_param1',
            '{{%item_has_param}}',
            'param_id'
        );

        // add foreign key for table `param`
        $this->addForeignKey(
            'fk_item_has_param_param1',
            '{{%item_has_param}}',
            'param_id',
            '{{%param}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `item`
        $this->dropForeignKey(
            'fk_item_has_param_item1',
            '{{%item_has_param}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            'fk_item_has_param_item1',
            '{{%item_has_param}}'
        );

        // drops foreign key for table `param`
        $this->dropForeignKey(
            'fk_item_has_param_param1',
            '{{%item_has_param}}'
        );

        // drops index for column `param_id`
        $this->dropIndex(
            'fk_item_has_param_param1',
            '{{%item_has_param}}'
        );

        $this->dropTable('{{%item_has_param}}');
    }
}
