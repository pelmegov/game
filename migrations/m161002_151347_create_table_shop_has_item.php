<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%shop_has_item}}`.
 */
class m161002_151347_create_table_shop_has_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%shop_has_item}}', [

            'id' => $this->primaryKey()->notNull(),
            'shop_id' => $this->integer(11)->notNull(),
            'item_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `shop_id`
        $this->createIndex(
            'fk_shop_has_item_shop1',
            '{{%shop_has_item}}',
            'shop_id'
        );

        // add foreign key for table `shop`
        $this->addForeignKey(
            'fk_shop_has_item_shop1',
            '{{%shop_has_item}}',
            'shop_id',
            '{{%shop}}',
            'id',
            'CASCADE'
        );

        // creates index for column `item_id`
        $this->createIndex(
            'fk_shop_has_item_item1',
            '{{%shop_has_item}}',
            'item_id'
        );

        // add foreign key for table `item`
        $this->addForeignKey(
            'fk_shop_has_item_item1',
            '{{%shop_has_item}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `shop`
        $this->dropForeignKey(
            'fk_shop_has_item_shop1',
            '{{%shop_has_item}}'
        );

        // drops index for column `shop_id`
        $this->dropIndex(
            'fk_shop_has_item_shop1',
            '{{%shop_has_item}}'
        );

        // drops foreign key for table `item`
        $this->dropForeignKey(
            'fk_shop_has_item_item1',
            '{{%shop_has_item}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            'fk_shop_has_item_item1',
            '{{%shop_has_item}}'
        );

        $this->dropTable('{{%shop_has_item}}');
    }
}
