<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
                require(__DIR__ . '/params.php'), require(__DIR__ . '/local/params-local.php')
);

$config = [
    'id' => 'app',
    'language' => 'ru-RU',
    'defaultRoute' => 'main/default/index',
    'components' => [
        'user' => [
            'class' => 'amnah\yii2\user\components\User',
        ],
        'errorHandler' => [
            'errorAction' => '/main/default/error',
        ],
        'request' => [
            'cookieValidationKey' => '',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'amnah\yii2\user\Module',
        // set custom module properties here ...
        ],
        'tree' => [
            'class' => 'app\modules\tree\Module',
        ],
        'pers' => [
            'class' => 'app\modules\pers\Module',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
